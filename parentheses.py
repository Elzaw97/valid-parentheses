
open_list = ["[", "{", "(","<"]
close_list = ["]", "}", ")",">"]



def check(expr):
    stack = []
    for i in expr:
        if i in open_list:
            stack.append(i)
        elif i in close_list:
            pos = close_list.index(i)
            if ((len(stack) > 0) and
                    (open_list[pos] == stack[len(stack) - 1])):
                stack.pop()
            else:
                return "The input is unvalid"
    if len(stack) == 0:
        return "The input is valid"


string = input("Enter a string to check:")
print(string, ":", check(string))


